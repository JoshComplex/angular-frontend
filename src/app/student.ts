export class Student {

    id: string;
    firstName: string;
    middleName: string;
    lastName: string;
    fullName: string;
    userName: string;
    email: string;

}
